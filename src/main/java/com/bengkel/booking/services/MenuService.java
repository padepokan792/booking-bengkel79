package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
    private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomerUsingStream();
    private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
    private static List<BookingOrder> listAllBookingOrders = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);
    private static String customerIdGeneral = "";

    public static void run() {
        boolean isLooping = true;
        do {
            startMenu();
            login();
            mainMenu();
        } while (isLooping);
    }

    public static void startMenu() {
        boolean isValid = false;
        do {
            System.out.println("=========== Aplikasi Booking Bengkel ===========");
            System.out.println("1. Login");
            System.out.println("0. Exit");
            int choice = Validation.validasiNumberWithRange("Masukkan pilihan: ", "Input harus berupa angka!", "^[0-9]+$", 1, 0);

            switch (choice) {
                case 1:
                    isValid = true;
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan coba lagi.");
                    break;
            }
        } while (!isValid);
    }

    public static void login() {
        int attempt = 1;
        boolean isLooping = true;

        do {
            System.out.println("=========== Login ===========");
            System.out.println("");
            System.out.println("Masukkan Customer Id:");
            String customerId = input.nextLine();
            System.out.println("Password:");
            String password = input.nextLine();
            boolean[] isValid = Validation.validasiLogin(customerId, listAllCustomers, password);

            if (isValid[0] && isValid[1]) {
                isLooping = false;
                customerIdGeneral = customerId;
            } else if (attempt == 3) {
                System.exit(0);
            }

            attempt++;
            System.out.println();
        } while (isLooping);
    }

    public static void mainMenu() {
        String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
        int menuChoice = 0;
        boolean isLooping = true;

        do {
            PrintService.printMenu(listMenu, "Selamat Datang di Aplikasi Booking Bengkel");
            menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu: ", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length - 1, 0);

            switch (menuChoice) {
                case 1:
                    // panggil fitur Informasi Customer
                    PrintService.printCustomer(getCustomerById(customerIdGeneral));
                    break;
                case 2:
                    // panggil fitur Booking Bengkel
					BengkelService.bookingBengkel(input, Validation.getCustomerById(customerIdGeneral, listAllCustomers).getVehicles(), listAllItemService, Validation.getCustomerById(customerIdGeneral, listAllCustomers), listAllBookingOrders);                    break;
                case 3:
                    // panggil fitur Top Up Saldo Coin
                    BengkelService.tambahSaldo(getCustomerById(customerIdGeneral));
                    break;
                case 4:
                    // panggil fitur Informasi Booking Order
                    PrintService.printBooking(listAllBookingOrders);
                    break;
                default:
                    System.out.println("Logout");
                    isLooping = false;
                    break;
            }
        } while (isLooping);
    }

    public static Customer getCustomerById(String customerId) {
        return listAllCustomers.stream()
                .filter(c -> c.getCustomerId().equalsIgnoreCase(customerId))
                .findFirst()
                .orElse(null);
    }
}

