package com.bengkel.booking.services;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Motorcyle;
import com.bengkel.booking.models.Vehicle;

public class Validation {
    
    public static String validasiInput(String question, String errorMessage, String regex) {
        Scanner input = new Scanner(System.in);
        String result;
        boolean isLooping = true;
        do {
            System.out.print(question);
            result = input.nextLine();

            //validasi menggunakan matches
            if (result.matches(regex)) {
                isLooping = false;
            } else {
                System.out.println(errorMessage);
            }

        } while (isLooping);

        return result;
    }

    public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
        int result;
        boolean isLooping = true;
        do {
            result = Integer.valueOf(validasiInput(question, errorMessage, regex));
            if (result >= min && result <= max) {
                isLooping = false;
            } else {
                System.out.println("Pilihan angka " + min + " s.d " + max);
            }
        } while (isLooping);

        return result;
    }

    public static boolean[] validasiLogin(String customerId, List<Customer> listAllCustomers, String password) {
        boolean isValid[] = {false, false};

        Optional<Customer> customerOpt = listAllCustomers.stream()
                .filter(customer -> customerId.equals(customer.getCustomerId()))
                .findFirst();

        if (customerOpt.isPresent()) {
            isValid[0] = true;
            if (password.equals(customerOpt.get().getPassword())) {
                isValid[1] = true;
            } else {
                System.out.println("Password Salah");
            }
        } else {
            System.out.println("Customer Id Tidak Ditemukan atau Salah");
        }

        return isValid;
    }

    public static Customer getCustomerById(String customerId, List<Customer> listAllCustomers) {
        return listAllCustomers.stream()
                .filter(customer -> customerId.equalsIgnoreCase(customer.getCustomerId()))
                .findFirst()
                .orElse(new Customer());
    }

    public static boolean validateVehicleById(String customerId, List<Vehicle> listAllCustomersVehicles) {
        boolean isValid = listAllCustomersVehicles.stream()
                .anyMatch(vehicle -> customerId.equalsIgnoreCase(vehicle.getVehiclesId()));

        if (!isValid) {
            System.out.println("Vehicle Id Tidak Diketahui");
        }

        return isValid;
    }

    public static String getVehicleType(String customerId, List<Vehicle> listAllCustomersVehicles) {
        return listAllCustomersVehicles.stream()
                .filter(vehicle -> customerId.equalsIgnoreCase(vehicle.getVehiclesId()))
                .map(vehicle -> vehicle instanceof Motorcyle ? "Motorcycle" : "Car")
                .findFirst()
                .orElse("");
    }

    public static boolean validateServiceById(String serviceId, List<ItemService> listAllService) {
        boolean isValid = listAllService.stream()
                .anyMatch(service -> serviceId.equalsIgnoreCase(service.getServiceId()));

        if (!isValid) {
            System.out.println("Service Id Tidak Diketahui");
        }
        return isValid;
    }

    public static boolean validate2Choice(String input, String compare1, String compare2) {
        boolean isValid = input.equalsIgnoreCase(compare1) || input.equalsIgnoreCase(compare2);

        if (!isValid) {
            System.out.println("Input Tidak Sesuai");
        }

        return isValid;
    }

    public static ItemService getServiceById(String serviceId, List<ItemService> listAllService) {
        return listAllService.stream()
                .filter(service -> serviceId.equalsIgnoreCase(service.getServiceId()))
                .findFirst()
                .orElse(new ItemService());
    }

    public static boolean isEnoughMoney(double moneyToPay, MemberCustomer customer) {
        boolean isEnoughMoney = customer.getSaldoCoin() > moneyToPay;

        if (!isEnoughMoney) {
            System.out.println("Saldo Tidak Cukup");
        }

        return isEnoughMoney;
    }
}
