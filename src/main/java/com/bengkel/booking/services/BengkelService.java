package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {
    private static int bookingOrderCounter = 1; 
	
    //Booking atau Reservation
    public static void bookingBengkel(Scanner input, List<Vehicle> listAllCustomersVehicles, List<ItemService> listServices, Customer customer, List<BookingOrder> listBookingOrders){
        String vehicleId, serviceId, choice, metodePembayaran = "Cash";
        int attempt = 1;
        double totalServicePrice = 0;
        List<ItemService> customerService = new ArrayList<>();
        boolean isLooping = true;

        do {
            System.out.println("Masukkan Vehicle Id:");
            vehicleId = input.nextLine();
        } while (!Validation.validateVehicleById(vehicleId, listAllCustomersVehicles));

        PrintService.printService(listServices, Validation.getVehicleType(vehicleId, listAllCustomersVehicles));

        do {
            do {
                System.out.println("Silahkan masukan Service Id:");
                serviceId = input.nextLine();
            } while (!Validation.validateServiceById(serviceId, listServices));

            customerService.add(Validation.getServiceById(serviceId, listServices));
            attempt++;

            if (customer.getMaxNumberOfService() != attempt) {
                do {
                    System.out.println("Apakah anda ingin menambahkan Service Lainnya? (Y/T)");
                    choice = input.nextLine();
                } while (!Validation.validate2Choice(choice, "Y", "T"));

                if (choice.equalsIgnoreCase("T")) {
                    isLooping = false;
                }
            } 

			if (customer instanceof MemberCustomer) {
				
				do {
					System.out.println("Silahkan Pilih Metode Pembayaran (Saldo Coin atau Cash)");
					metodePembayaran = input.nextLine();
				} while (!Validation.validate2Choice(metodePembayaran, "Saldo Coin", "Cash"));
	
				if (metodePembayaran.equalsIgnoreCase("Saldo Coin")) {
					boolean isEnoughMoney = Validation.isEnoughMoney(totalServicePrice, (MemberCustomer)customer);
	
					if (!isEnoughMoney) {
						System.out.println("Saldo Tidak Cukup, Akan Dialihkan Ke Pembayaran Cash");
						metodePembayaran = "Cash";
					} else {
						double saldoCoinCustomer = ((MemberCustomer)customer).getSaldoCoin();
						((MemberCustomer)customer).setSaldoCoin(saldoCoinCustomer -= totalServicePrice);
					}
				}
			}

        } while (customer.getMaxNumberOfService() != attempt && isLooping);

        for (ItemService itemService : customerService) {
            totalServicePrice += itemService.getPrice();
        }

        BookingOrder bookingOrder = new BookingOrder(generateBookingOrderId(customer.getCustomerId()), customer, customerService, metodePembayaran, totalServicePrice, 0);
        bookingOrder.calculatePayment();
        listBookingOrders.add(bookingOrder);

		System.out.println("Booking Berhasil!!!");
        System.out.println("Total Harga Service : " + String.format("%,.2f", totalServicePrice));
        System.out.println("Total Harga Pembayaran : " + String.format("%,.2f", bookingOrder.getTotalPayment()));
        System.out.println();
    }

    public static String generateBookingOrderId(String customerId) {
        String bookingOrderId = "Book-" + customerId + "-" + String.format("%03d", bookingOrderCounter);
        bookingOrderCounter++;
        return bookingOrderId;
    }
    
    // Top Up Saldo Coin Untuk Member Customer
    public static void tambahSaldo(Customer customer){
        if (customer instanceof MemberCustomer) {
            double saldoTambah = Double.parseDouble(Validation.validasiInput("Masukan Jumlah Saldo Yang Ditambahkan: ", "Input Harus Berupa Angka!", "^[0-9]+$"));
            double saldo = ((MemberCustomer)customer).getSaldoCoin();
            ((MemberCustomer)customer).setSaldoCoin(saldo + saldoTambah);
        } else {
            System.out.println("Maaf Fitur Ini Hanya Untuk Member");
        }
        System.out.println();
    }
    
    //Logout
    
}
