package com.bengkel.booking.repositories;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.bengkel.booking.models.ItemService;

public class ItemServiceRepository {
    
    public static List<ItemService> getAllItemService() {
        return Arrays.asList(
                new ItemService("Svcm-001", "Ganti Oli", "Motorcycle", 70000),
                new ItemService("Svcm-002", "Service Mesin", "Motorcycle", 150000),
                new ItemService("Svcm-003", "Service CVT", "Motorcycle", 50000),
                new ItemService("Svcm-004", "Ganti Ban", "Motorcycle", 200000),
                new ItemService("Svcm-005", "Ganti Rem", "Motorcycle", 90000),
                new ItemService("Svcc-001", "Ganti Oli", "Car", 170000),
                new ItemService("Svcc-002", "Service Mesin", "Car", 500000),
                new ItemService("Svcc-003", "Service AC", "Car", 250000),
                new ItemService("Svcc-004", "Isi Freon AC", "Car", 70000),
                new ItemService("Svcc-005", "Cuci Mobil", "Car", 50000)
        );
    }
    
    public static List<ItemService> getAllItemServiceUsingStream() {
        return getAllItemService().stream()
                .collect(Collectors.toList());
    }
}
